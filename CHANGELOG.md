# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.2](https://gitlab.com/spartanbio-ux/stylelint-config/compare/v1.0.1...v1.0.2) (2019-08-26)

### [1.0.1](https://gitlab.com/spartanbio-ux/stylelint-config/compare/v1.0.0...v1.0.1) (2019-06-17)


### Bug Fixes

* adds readme ([54cd61c](https://gitlab.com/spartanbio-ux/stylelint-config/commit/54cd61c))



## 1.0.0 (2019-06-17)
